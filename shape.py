import time
from diffusers import DiffusionPipeline
from diffusers.utils import export_to_ply
from PIL import Image
from flask import Flask, Response, jsonify, request
import uuid
import hashlib
import os
from threading import Thread

t2i_pipe = DiffusionPipeline.from_pretrained("openai/shap-e", trust_remote_code=True)

global queue

queue = []

def iap2m(prompt: str):
    res = t2i_pipe(
        #image=img,
        prompt=prompt,
        guidance_scale=0.5,
        num_inference_steps=64,
        frame_size=256,
        output_type="mesh"
    )
    return res.images[0]

def findfirstnotinprogress():
    lastWoAllProgress = None
    for itm in queue:
        if itm["status"] == 0:
            lastWoAllProgress = itm
            break
    return lastWoAllProgress

def markcompleted(id):
    for key, value in queue:
        if value["id"] == id:
            queue[key]["status"] = 127

def bgthread():
    while True:
        fnip = findfirstnotinprogress()
        if fnip:
            tmpimg = iap2m(fnip["prompt"])
            f = open(tmpimg, "rb")
            imgbin = f.read()
            f.close()
            hash2 = hashlib.sha256(fnip["id"]).hexdigest()
            f = open("./" + hash2 + ".ply", "wb")
            f.write(imgbin)
            f.flush()
            f.close()
            markcompleted(fnip["id"])
        else:
            time.sleep(0.1)


app = Flask(__name__)

@app.route("/queue_mesh", methods=["POST"])
def queue_mesh():
    try:
        data = request.get_json(force=True)
    except e:
        print(e)
        return jsonify({
            "status": "error",
            "message": "malformed input"
        })
    if not "prompt" in data:
        return jsonify({
            "status": "error",
            "message": "missing prompt"
        })
    if len(queue) > 10:
        return jsonify({
            "status": "error",
            "message": "queue full. Try again in 30 minutes."
        })
    quid = uuid.uuid4()
    queue.append({
        "id": quid,
        "prompt": data["prompt"],
        "status": 0
    })
    return jsonify({
        "status": "ok",
        "id": quid,
        "message": "success"
    })

@app.route("/queue_status/<quid>", methods=["GET"])
def queue_status(quid):
    itm = None
    for it in queue:
        if it["id"] == quid:
            itm = it
            break
    if not itm:
        return jsonify({
            "status": "error",
            "message": "no such queued prompt id \"" + quid + "\""
        })
    return jsonify({
        "status": "ok",
        "status": itm["status"],
        "message": "queued"
    })

@app.route("/finished/<id>", methods={"GET"})
def finished(id):
    fnm = hashlib.sha256(id).hexdigest()
    if not os.path.exists(fnm + ".ply"):
        return jsonify({
            "status": "error",
            "message": "no such model"
        })
    f = open(fnm + ".ply", "rb")
    ply = f.read()
    f.close()
    return Response(ply, mimetype="application/octet-stream")

if __name__ == "__main__":
    t = Thread(target=bgthread, daemon=True)
    t.start()
    app.run(host="0.0.0.0", port=8080)